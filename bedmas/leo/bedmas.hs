import Data.List

type SingleRule = (Char, Bool)
type RuleSet = [SingleRule]

initRules :: Num a => a -> RuleSet
initRules _ = []

addRule :: RuleSet -> SingleRule -> RuleSet
addRule set rule = set ++ [rule]

type OperatorSet = [Char]

operators :: RuleSet -> OperatorSet
operators [] = []
operators (rule:rules) = (fst rule):(operators rules)

splitCount :: (Num a, Eq a) => (Char -> a) -> OperatorSet -> a -> String -> (String, String)
splitCount _ _ _ "" = ("", "")
splitCount f ops 0 (c:rem) =
    if c `elem` ops then ("", c:rem)
    else let (rexpr, rrem) = splitCount f ops (f c) rem in (c:rexpr, rrem)
splitCount f ops n (c:rem) =
        (let (pexpr, rrem) = splitCount f ops (f c + n) rem in
            ((c:pexpr), rrem))

countPar :: Num a => Char -> a
countPar c
    | c == '(' = 1
    | c == ')' = -1
    | otherwise = 0

splitCountR = splitCount countPar

splitCountL :: (Num a, Eq a) => OperatorSet -> a -> String -> (String, String)
splitCountL ops n input = let (expr, rem) = splitCount (negate . countPar) ops n (reverse input) in
    (reverse rem, reverse expr)

splitExpr :: OperatorSet -> String -> String -> String -> (String, String, String, String, String)
splitExpr ops prev expr after = let (lhs, lexpr) = splitCountL ops 0 prev; (rexpr, rhs) = splitCountR ops 0 after
    in (lhs, lexpr, expr, rexpr, rhs)

applyRuleRepeatLeftAssoc :: String -> OperatorSet -> Char -> String -> String
applyRuleRepeatLeftAssoc res _ _ "" = res
applyRuleRepeatLeftAssoc res ops char (x:rem) =
    if x == char
    then let (lhs, elhs, expr, erhs, rhs) = splitExpr ops res [x] rem in
         applyRuleRepeatLeftAssoc (concat [lhs, "(", elhs, [x]]) ops char (concat [erhs, ")", rhs])
    else applyRuleRepeatLeftAssoc (res ++ [x]) ops char rem

applyRuleRepeatRightAssoc :: String -> OperatorSet -> Char -> String -> String
applyRuleRepeatRightAssoc res _ _ "" = res
applyRuleRepeatRightAssoc res ops char input =
    let rem = init input; x = last input in
        if x == char
        then let (lhs, elhs, expr, erhs, rhs) = splitExpr ops rem [x] res in
             applyRuleRepeatRightAssoc (concat [[x], erhs, ")", rhs]) ops char (concat [lhs, "(", elhs])
        else applyRuleRepeatRightAssoc (x:res) ops char rem

applyRuleLA = applyRuleRepeatLeftAssoc ""
applyRuleRA = applyRuleRepeatRightAssoc ""

parseRule :: String -> SingleRule
parseRule (op:sep:assoc) = (op, assoc == "left")

showDisambiguated :: OperatorSet -> RuleSet -> String -> String
showDisambiguated _ _ "" = ""
showDisambiguated _ [] expr = expr
showDisambiguated ops ((char, assoc):rules) expr =
    let applyRule = if assoc then applyRuleLA else applyRuleRA in
    showDisambiguated ops rules (applyRule ops char expr)

parseLines :: RuleSet -> [String] -> String
parseLines rules [expr] = showDisambiguated (operators rules) rules expr
parseLines rules (line:lines) = parseLines (addRule rules (parseRule line)) lines

disambiguate :: [String] -> String
disambiguate [] = ""
disambiguate (x:xs) = parseLines (initRules (read x :: Int)) xs
