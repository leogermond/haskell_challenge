import Data.List

warmup1 :: (Num a, Eq a) => [a] -> [a]
warmup1 [] = []
warmup1 (0:xs) = warmup1 xs
warmup1 (x:xs) = x : warmup1 xs

warmup2 :: Ord a => [a] -> [a]
warmup2 = sortBy (flip compare)

warmup3 :: (Num a, Ord a, Foldable t) => a -> t b -> Bool
warmup3 x y = x > (fromIntegral (length y)) 

warmup4 :: (Num a, Eq a, Num b) => a -> [b] -> [b]
warmup4 0 x = x
warmup4 _ [] = []
warmup4 n (x:xs) = (x - 1) : (warmup4 (n - 1) xs)

havel_hakimi :: (Num a, Eq a, Ord a) => [a] -> Bool
havel_hakimi x =
    case (warmup2 (warmup1 x)) of
        [] -> True
        (n:x) -> if warmup3 n x
               then False
               else havel_hakimi (warmup4 n x)
